//
// Created by jmu on 18-7-25.
//

#ifndef AUDIO_DECODE_LOG_H
#define AUDIO_DECODE_LOG_H

#include <stdio.h>
#include <libavutil/error.h>

#define LOG_TAG "log.h"
#define LOG(format, args... ) fprintf(stdout, "[%s:%d] " format "\n", LOG_TAG, __LINE__, ##args );
#define LOGE(format, args... ) fprintf(stderr, "[%s:%d] " format "\n", LOG_TAG, __LINE__, ##args );

/*
 * 生成判断条件: retCode condition, 进行输出错误信息
 * 如: CHECK(x, >0)生成 if(x>0)
 */
#define ffmpeg_check(retCode, condition)               \
    if (retCode condition) {                  \
        LOGE("error: %s", av_err2str(retCode))   \
    }

#endif //AUDIO_DECODE_LOG_H
