//
// Created by jmu on 18-7-25.
//
#include <stdio.h>
#include <libavformat/avformat.h>
#include <libswresample/swresample.h>
#include "log.h"

#define LOG_TAG "audio_decode.c"

static void save_frame(SwrContext *swrContext, AVFrame *audioFrame);

static FILE *file;
static void *_swrBuff;
static int _swrBuffSize = 0;

int main(int argc, char *args[]) {
    if (argc < 3) {
        printf("usage: %s input output.pcm\n", args[0]);
        return 1;
    }

    file = fopen(args[2], "w+");
    if (file == NULL) {
        LOGE("%s", strerror(errno))
        return 1;
    }

    _swrBuff = malloc((size_t) _swrBuffSize);


    LOG("%s", args[1])

    //注册
    av_register_all();
    AVFormatContext *avFormatCtx = avformat_alloc_context();

    //打开文件
    int ret;
    ret = avformat_open_input(&avFormatCtx, args[1], NULL, NULL);
    ffmpeg_check(ret, !=0)
    av_dump_format(avFormatCtx, 0, args[1], 0);

    //查找音频流
    int audioIndex = -1;
    for (int i = 0; i < avFormatCtx->nb_streams; ++i) {
        AVStream *stream = avFormatCtx->streams[i];
        if (stream->codec->codec_type == AVMEDIA_TYPE_AUDIO) {
            audioIndex = i;
        }
    }
    if (audioIndex == -1) {
        LOG("查找不到音频流")
        goto end;
    }
    AVStream *audioStream = avFormatCtx->streams[audioIndex];
    AVCodecContext *avCodecCtx = audioStream->codec;


    //查找解码器
    AVCodec *decoder = avcodec_find_decoder(avCodecCtx->codec_id);
    if (decoder == 0) {
        LOG("查找不到解码器")
        goto end;
    }

    //打开解码器
    ret = avcodec_open2(avCodecCtx, decoder, NULL);
    ffmpeg_check(ret, <0)

    //构建解码格式和解码后数据存储结构体
    SwrContext *swrContext = NULL;
    if (avCodecCtx->sample_fmt != AV_SAMPLE_FMT_S16) {
        swrContext = swr_alloc_set_opts(NULL,
                                        AV_CH_LAYOUT_MONO, AV_SAMPLE_FMT_S16, 44100,
                                        avCodecCtx->channel_layout, avCodecCtx->sample_fmt, avCodecCtx->sample_rate,
                                        0, NULL);
        if (swrContext == NULL || swr_init(swrContext)) {
            if (swrContext) {
                swr_free(&swrContext);
            }
        }
    }
    AVFrame *audioFrame = av_frame_alloc();

    // 打开了解码器之后，就可以读取一部分流中的数据（压缩数据），然后将压缩数据作
    //    为解码器的输入，解码器将其解码为原始数据（裸数据），之后就可以将原始数据写
    //            入文件了

    AVPacket avPacket;
    int got_frame;
    while (1) {
        if (av_read_frame(avFormatCtx, &avPacket) < 0) {
            //end of file
            break;
        }
        if (avPacket.stream_index == audioIndex) {
            int len = avcodec_decode_audio4(avCodecCtx, audioFrame, &got_frame, &avPacket);
            if (len < 0) {
                break;
            }

            if (got_frame) {
                save_frame(swrContext, audioFrame);
            }
        }
    }

    end:
    avformat_free_context(avFormatCtx);
    return 0;
}

void save_frame(SwrContext *swrContext, AVFrame *audioFrame) {
    void *audioData;
    int numFrames;
    int channels = 2;
    if (swrContext) {
        int bufSize = av_samples_get_buffer_size(NULL, channels,
                                                 audioFrame->nb_samples * channels,
                                                 AV_SAMPLE_FMT_S16, 1);
        if (_swrBuff || _swrBuffSize < bufSize) {
            _swrBuffSize = bufSize;
            _swrBuff = realloc(_swrBuff, (size_t) _swrBuffSize);
        }

        uint8_t *output[2] = {_swrBuff, 0};
        numFrames = swr_convert(swrContext, output,
                                audioFrame->nb_samples * channels,
                                (const uint8_t **) audioFrame->data,
                                audioFrame->nb_samples);
        audioData = _swrBuff;
    } else {
        audioData = audioFrame->data[0];
        numFrames = audioFrame->nb_samples;
    }
    fwrite(audioData, 1, (size_t) (numFrames * channels), file);

}